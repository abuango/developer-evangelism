# Developer Evangelism

Hi, I am Abubakar, welcome to my Developer Evngelism journey at GitLab. I am currently the Technical Evagelism Program Manager and also evangelize GitLab with a focus on Kubernetes and the Cloud Native ecosystem. You can follow all my activities on my [personal website](https://abuango.me).

## Purpose
This project tracks my plans and progress as a Developer Evangelist. `In 2020, my main goal is to become a known contributor in the Kubernetes community.`  The plans are divided into 2 broad goals

- Build a thought leadership in the Kubernetes and Cloud native ecosystem
- Help build a solid support system for Developer Evangelists, the GitLab team and wider community members towards building thought leadership.

All the yearly plans listed below will be designed towards achieving the aforementioned goals.

## Focus Area

### Audience

Part of the team's objectives is to reach new audiences and get as many eye balls as possible on the content we create as a team and as the Technical Evangelism Program Manager, my work will include the following:
- Seek new ways to get more audience for teh content we create
- Initiate processes that make it easier for the team to achieve their the team's objectives and individual plans
- Coordinate programs, CFPs, and other initiatives around engaging with our audience.

### My Focus

#### Kubernetes

My primary focus is Kubernetes/Cloud Native. With experience using and managing Kubernetes clusters, supporting GitLab's customers using Kubernetes in the their development lifecycles with GitLab and as a Certified kubenetes Administrator, I will be building thought leadership in the Cloud Native ecosystem, creating content, sharing opinions and contributing to projects like Kubernetes, Helm and others.

#### Contributions

- Join Kubernetes SIGs:
    - [sig-release](https://github.com/kubernetes/community/blob/master/sig-release/README.md)
    - [sig-docs](https://github.com/kubernetes/community/blob/master/sig-docs/README.md)
    - [sig-contribex](https://github.com/kubernetes/community/blob/master/sig-contributor-experience/README.md)
- Join the [Helm Community](https://github.com/helm/community): Helm is currently the most popular way of installing applications in Kubernetes clusters, engagement in the community will put in the forefront of cloud native application developments.
- [Continous Delivery Foundation](https://cd.foundation/)
- Shadow GitLab teams involved in the decision making / development of Cloud Native features and create stories to share from the learnings:
    - [g_distribution](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/)
    - [g_delivery](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/)
    - [g_scalability](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/)

#### Community

Active participation in communities is a major way of building thought leadership, I will be leveraging my current enagegemnts in the Google Developer Groups (GDG) and the Kubernetes Community to build thought leadership and actively participate as follows:
- Events / Meetups
    - Kubernetes Developer Days
    - KubeCon
    - FOSDEM
    - Helm Summit
    - Continous Delivery Summit
    - [GDG Cloud Netherlands](https://www.meetup.com/gdgcloudnl/)
- Community Engagements
    - CNCF Ambassador
    - Google Developer Expert


#### Content

Create quality content and show thought leadership through mediums like:
- DZone
- Dev.to
- Medium
- Twitter
- LinkedIn
- Youtube
- Podcasts
- Kubernetes Blog
- Partner's Blog (Google, AWS, etc.)

### Metrics

To successfully measure progress, properly recording and reporting activities will is crucial. I will be dogfooding GitLab using [Epics](https://docs.gitlab.com/ee/user/group/epics/), [Milestones](https://docs.gitlab.com/ee/user/project/milestones/) and [Issues](https://docs.gitlab.com/ee/user/project/issues/)

Tentative metrics include:
- At least 2 Monthly articles on (DZone, Medium, Dev.to or any other medium)
- 1 GitLab Blog post
- At least 1 MR to GitLab upstream
- At least 5 MRs to community projects like k8s, Helm, etc.

## Yearly Plans

- [FS 2020](https://gitlab.com/abuango/developer-evangelism/-/blob/master/2020/README.md)
